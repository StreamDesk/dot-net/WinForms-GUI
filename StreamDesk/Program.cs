﻿#region Licensing Information
/***************************************************************************************************
 * NasuTek StreamDesk
 * Copyright © 2007-2012 NasuTek Enterprises
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***************************************************************************************************/
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows.Forms;
using NasuTek.M3.Streams;
using NasuTek.M3.Streams.DatabaseFormats;

namespace StreamDesk {
    internal static class Program {
        internal static StreamsCore Database { get; set; }
        internal static StreamsSettings Settings { get; private set; }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread] private static void Main(string[] args) {
            Application.SetCompatibleTextRenderingDefault(false);
            Application.EnableVisualStyles();

            Database = new StreamsCore();

            if (File.Exists(Path.Combine(Application.StartupPath, "StreamDesk.Compatability.dll"))) {
                Assembly compatDict = Assembly.LoadFile(Path.Combine(Application.StartupPath, "StreamDesk.Compatability.dll"));
                var sdFormatter = (IDatabaseFormatter)Activator.CreateInstance(compatDict.GetType("StreamDesk.Compatability.DatabaseFormats.SdClassicXmlFormatter"));
                var sdFormatterBinary = (IDatabaseFormatter)Activator.CreateInstance(compatDict.GetType("StreamDesk.Compatability.DatabaseFormats.SdClassicBinaryFormatter"));

                StreamsCore.FormatterEngine.Formatters.Add(sdFormatter);
                StreamsCore.FormatterEngine.Formatters.Add(sdFormatterBinary);
            }

            StreamsCore.FormatterEngine.Formatters.Add(new XmlFormatter());

            if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "StreamDesk")))
                Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "StreamDesk"));

            Settings = StreamsSettings.OpenSettings();
            if (Settings.ActiveDatabases.All(d => d != "http://app.streamdesk.ca/streams.sdnx"))
                Settings.ActiveDatabases.Add("http://app.streamdesk.ca/streams.sdnx");

            Application.Run(new LoadDatabases());
            Settings.SaveSettings();
        }
    }
}
